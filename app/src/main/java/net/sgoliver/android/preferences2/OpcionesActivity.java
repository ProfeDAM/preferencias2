package net.sgoliver.android.preferences2;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;

public class OpcionesActivity extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.opciones);

        CheckBoxPreference cbPreference = (CheckBoxPreference) findPreference("opcion1");
        cbPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference preference) {
                Toast.makeText(OpcionesActivity.this, "Me has cambiado", Toast.LENGTH_LONG).show();
                return false;
            }
        });


    }


}
