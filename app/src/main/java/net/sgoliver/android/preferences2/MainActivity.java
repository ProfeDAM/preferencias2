package net.sgoliver.android.preferences2;

import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends Activity {

	private Button btnPreferencias;
	private Button btnObtenerPreferencias;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		changeColor();


		btnPreferencias = (Button) findViewById(R.id.BtnPreferencias);
		btnObtenerPreferencias = (Button) findViewById(R.id.BtnObtenerPreferencias);

		btnPreferencias.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(MainActivity.this,
						OpcionesActivity.class), 1000);
			}
		});

		btnObtenerPreferencias.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SharedPreferences pref =
						PreferenceManager.getDefaultSharedPreferences(
								MainActivity.this);

				Log.i("info", "Opci�n 1: " + pref.getBoolean("opcion1", false));
				Log.i("info", "Opci�n 2: " + pref.getString("opcion2", ""));
				Log.i("info", "Opci�n 3: " + pref.getString("opcion3", ""));




			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		changeColor();


	}

	public void changeColor() {
		SharedPreferences pref =
				PreferenceManager.getDefaultSharedPreferences(
						MainActivity.this);

		LinearLayout linearLayout = findViewById(R.id.layout);


		String color = pref.getString("opcion3", "BLUE");
		switch (color) {
			case "BLUE": //Pintad el layour de Color.Blue
				linearLayout.setBackgroundColor(Color.BLUE);
				break;
			case "YELLOW":
				linearLayout.setBackgroundColor(Color.YELLOW);
				break;
			case "RED":
				linearLayout.setBackgroundColor(Color.RED);
				break;
		}
	}
}
